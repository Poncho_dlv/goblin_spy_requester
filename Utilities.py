#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import math
from spike_database.MatchHistory import MatchHistory
from spike_database.BB2Resources import BB2Resources
from spike_database.ResourcesRequest import ResourcesRequest
from goblin_spy_requester.GoblinSpyRequester import GoblinSpyRequester
from spike_requester.SpikeAPI.CclAPI import CclAPI

spike_logger = logging.getLogger("spike_logger")


class Utilities:

    def __init__(self):
        self.__failed_competitions = []

    def save_cabalvision_history_into_db(self, season_number: int, platform_id: int, nb_match: int):
        match_history_db = MatchHistory()
        league_name = "Cabalvision Official League"
        rsrc_db = ResourcesRequest()
        platform = rsrc_db.get_platform_label(platform_id)
        data = CclAPI.get_seasons(platform_id)
        competition_name = data.get("ladders")[season_number-1]["name"]
        spike_logger.info("Save data from {} {}".format(competition_name, platform))
        self.add_competition(competition_name, league_name, platform, match_history_db, nb_match)

    def save_competitions_history_into_db(self, nb_match: int):
        requester = GoblinSpyRequester()
        json_data = requester.get_competitions_history()
        match_history_db = MatchHistory()
        match_history_db.open_database()
        if json_data is not None:
            for league in json_data.get("leagues", []):
                league_name = league.get("name", "")
                if league_name != "" and league_name != "Cabalvision Official League":
                    platform = league.get("platform")
                    if platform != "":
                        for competition in league.get("list", []):
                            competition_name = competition.get("name", "")
                            if competition_name != "":
                                self.add_competition(competition_name, league_name, platform, match_history_db, nb_match)
                            else:
                                spike_logger.info("Ignore empty competition name for: {} - {}".format(league_name, platform))
                    else:
                        spike_logger.info("Ignore empty platform for: {}".format(league_name))
                else:
                    spike_logger.info("Ignore empty league name")
        return self.__failed_competitions

    def add_competition(self, competition_name: str, league_name: str, platform: str, match_history_db: MatchHistory, nb_match: int):
        try:
            bb_db = BB2Resources()
            rsrc_db = ResourcesRequest()
            platform_id = rsrc_db.get_platform_id(platform)
            requester = GoblinSpyRequester()
            matches = {}
            matches_data = requester.get_match_history(league_name, competition_name, platform, nb_match)
            if matches_data is not None and matches_data.get("IsSuccess", False) is True:
                spike_logger.info("Save history for: {} - {} - {}".format(league_name, competition_name, platform))
                matches_data = matches_data.get("Data", {}).get("rows", [])
                for match in matches_data:
                    race_id_home = bb_db.get_race_id(match[9])
                    race_id_away = bb_db.get_race_id(match[16])

                    matches[match[0]] = {
                        "_id": match[0],
                        "competition_name": competition_name,
                        "league_name": league_name,
                        "platform": int(platform_id),
                        "date": match[3],
                        "duration": int(match[4]),
                        "team_home": {
                            "name": match[11],
                            "tv": int(match[1]),
                            "coach_id": int(match[5].split("-")[1]),
                            "conceded": int(match[7]),
                            "race_id": race_id_home,
                            "logo": match[10],
                            "score": int(match[12])
                        },
                        "team_away": {
                            "name": match[14],
                            "tv": int(match[2]),
                            "coach_id": int(match[6].split("-")[1]),
                            "conceded": int(match[18]),
                            "race_id": race_id_away,
                            "logo": match[15],
                            "score": int(match[13])
                        },
                    }
                existing_matches = match_history_db.matches_exist(list(matches.keys()))
                spike_logger.info("Existing matches: {}".format(len(existing_matches)))
                save_match = []
                for match in matches.values():
                    if match.get("_id") not in existing_matches:
                        save_match.append(match)
                if len(save_match) > 0:
                    spike_logger.info("New matches: {}".format(len(save_match)))
                    match_history_db.insert_matches(save_match)
            else:
                tpl = (league_name, competition_name, platform)
                self.__failed_competitions.append(tpl)
                spike_logger.info("Unable to save history for: {} - {} - {}".format(league_name, competition_name, platform))
        except Exception as e:
            spike_logger.error("Error: {}".format(e))

    @staticmethod
    def get_safe_path(league_name: str, competition_name: str, platform: str):
        data = Utilities.to_utf8_array(league_name + " - " + competition_name + " - " + platform)
        base32_standard_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        result = ""
        idx = 0
        while idx < len(data):
            bytes_idx = min(len(data) - idx, 5)
            buff = list(data[idx:idx + bytes_idx])
            buff.reverse()
            while len(buff) < 8:
                buff.append(0)
            buff.reverse()
            bit_offset = ((bytes_idx + 1) * 8) - 5
            while bit_offset > 3:
                byte_offset = math.floor(bit_offset / 8)
                bits_left = bit_offset - byte_offset * 8
                sub_buff = buff[len(buff) - byte_offset - 1: len(buff) - byte_offset - 1 + 2]
                if len(sub_buff) > 1:
                    val = (sub_buff[0] << 8) + sub_buff[1]
                else:
                    val = sub_buff[0] << 8
                shifted = (val >> bits_left)
                bit_val = shifted & 0x1f
                result += base32_standard_alphabet[bit_val]
                bit_offset -= 5
            idx += 5
        return result

    @staticmethod
    def to_utf8_array(input_txt: str):
        output = []
        for char in input_txt:
            output.append(ord(char))
        return output
