#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import logging
import time

import requests

from goblin_spy_requester import goblin_spy_requester_logger

spike_logger = logging.getLogger("spike_logger")

base_url = "http://www.mordrek.com:8888/"
base_query_url = base_url + "goblinSpy/web/Query?query={}"


class GoblinSpyRequester:

    @staticmethod
    def get_competitions_history():
        competition_history = "http://www.mordrek.com/goblinSpy/Overview/competitions.json"
        json_data = GoblinSpyRequester.send_request(competition_history)
        return json_data

    @staticmethod
    def register_competition(league_name: str, competition_name: str, platform: str):
        url = base_url + "EnableCollection?platform={}&league={}&competition={}".format(platform, league_name, competition_name)
        return GoblinSpyRequester.send_request(url)

    @staticmethod
    def register_contest(league_name: str, competition_name: str, platform: str, contest_id: int, date: str):
        register_contest_query = base_url + "SetUserSchedule?platform={}&league={}&competition={}&contest_id={}&new_date={}".format(platform, league_name, competition_name, contest_id, date)
        return GoblinSpyRequester.send_request(register_contest_query)

    @staticmethod
    def set_competition_sorting(league_name: str, competition_name: str, platform: str, sorting: str):
        set_sorting_query = base_url + "SetCompetitionSorting?platform={}&league={}&competition={}&sorting={}".format(platform, league_name, competition_name, sorting)
        return GoblinSpyRequester.send_request(set_sorting_query)

    @staticmethod
    def get_competition_sorting(league_name: str, competition_name: str, platform: str):
        json_data = GoblinSpyRequester.get_competitions_history()
        for league in json_data["leagues"]:
            if league["name"] == league_name and league["platform"] == platform:
                for competition in league["list"]:
                    if competition["name"] == competition_name:
                        return competition["sorting"]
        return None

    @staticmethod
    def get_competition_format(league_name: str, competition_name: str, platform: str):
        json_data = GoblinSpyRequester.get_competitions_history()
        for league in json_data["leagues"]:
            if league["name"] == league_name and league["platform"] == platform:
                for competition in league["list"]:
                    if competition["name"] == competition_name:
                        return competition["format"]
        return None

    @staticmethod
    def get_standing(league_name: str, competition_name: str, platform: str, limit=50):
        filename = "goblin_spy_requester/requests/standing.json"
        with open(filename, encoding="utf-8") as file:
            json_data = json.load(file)
            args = json.dumps(json_data)
            url = base_query_url.format(args)
            url = url.replace("%COMPETITION_NAME%", competition_name)
            url = url.replace("%LEAGUE_NAME%", league_name)
            url = url.replace("%PLATFORM%", platform)
            url = url.replace("%LIMIT%", str(limit))
            return GoblinSpyRequester.send_request(url)

    @staticmethod
    def get_standing_link(league_name: str, competition_name: str, platform: str):
        standing_link = "http://www.mordrek.com/goblinSpy/web/goblinSpy.html?platform={}&league={}&competition={}&q=*front".format(platform, league_name, competition_name)
        return standing_link

    @staticmethod
    def get_coach_link(coach_name: str, platform: str):
        coach_link = "http://www.mordrek.com/goblinSpy/web/goblinSpy.html?platform={}&q=*coach&c={}".format(platform, coach_name)
        return coach_link

    @staticmethod
    def get_match_history(league_name: str, competition_name: str, platform: str, nb_match: int):
        filename = "goblin_spy_requester/requests/match_history.json"
        with open(filename, encoding="utf-8") as file:
            json_data = json.load(file)
            args = json.dumps(json_data)
            url = base_query_url.format(args)
            url = url.replace("%COMPETITION_NAME%", competition_name)
            url = url.replace("%LEAGUE_NAME%", league_name)
            url = url.replace("%PLATFORM%", platform)
            url = url.replace("%LIMIT%", str(nb_match))
            return GoblinSpyRequester.send_request(url, 60)

    @staticmethod
    def get_contests(league_name: str, competition_name: str, platform: str, nb_match):
        filename = "goblin_spy_requester/requests/schedule.json"
        with open(filename, encoding="utf-8") as file:
            json_data = json.load(file)
            args = json.dumps(json_data)
            url = base_query_url.format(args)
            url = url.replace("%COMPETITION_NAME%", competition_name)
            url = url.replace("%LEAGUE_NAME%", league_name)
            url = url.replace("%PLATFORM%", platform)
            url = url.replace("%LIMIT%", str(nb_match))
            return GoblinSpyRequester.send_request(url)

    @staticmethod
    def request_missing_data(league_name: str, competition_name: str, platform: str, date_from: str, date_to: str):
        query = base_url + "RequestMissingData?platform={}&league={}&competition={}&from={}&to={}".format(platform, league_name, competition_name, date_from, date_to)
        return GoblinSpyRequester.send_request(query)

    @staticmethod
    def mark_duplicate(match_uuid: str, home_team_id: int):
        query = base_url + "MarkDuplicate?idteamstats={}-{}".format(match_uuid, home_team_id)
        return GoblinSpyRequester.send_request(query)

    @staticmethod
    def send_request(url, timeout: int = 15):
        ret = None
        try:
            start_time = time.time()
            get_request = requests.get(url, timeout=timeout)
            if get_request.status_code == 200:
                duration = time.time() - start_time
                goblin_spy_requester_logger.info("GET :: {} :: {}".format(url[:50], duration))
                ret = get_request.json()
            else:
                spike_logger.error("get_request error :: url: {} :: status_code {}".format(url[:50], get_request.status_code))
        except Exception as e:
            spike_logger.error("get_request error :: url: {} :: error {}".format(url[:50], e))
        return ret
