#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from logging.handlers import RotatingFileHandler


goblin_spy_requester_logger = logging.getLogger("goblin_spy_requester")
goblin_spy_requester_logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt="%(asctime)s :: %(levelname)s :: %(message)s", datefmt="%Y-%m-%dT%H:%M:%S%z")
file_handler = RotatingFileHandler("log/goblin_spy_requester.log", maxBytes=10000000, backupCount=10, encoding='utf-8')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
goblin_spy_requester_logger.addHandler(file_handler)

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
goblin_spy_requester_logger.addHandler(stream_handler)
